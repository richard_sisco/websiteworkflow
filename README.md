# WebsiteWorkflow

## Description

This project is an [Alfred]('https://www.alfredapp.com/') workflow that creates shortcuts for opening URL sites associated with a "project" tag and identified with "request" tags.

## Setup

* Download the repo to your local drive and note the location (e.g. /Server/sites/websiteworkflow).
* Edit `sites.xml` to create nodes for each site following the example therein.
* When adding a new site (e.g. "exa"), the Alfred workflow must be edited to define the prefix and its usage. Alternatively, you can just prefix your Alfred command with "open" (e.g. "open exa pa").

## Usage

The Alfred shortcut "exa pa" would open the production admin URL for the site assigned to the "exa" tag while the shortcut "exa repo" would open its repository URL.

Tags available for use can be found by reviewing `requestTags.xml` and adding new tags may be done in the same place. 