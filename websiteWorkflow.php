<?php
// Set these two variables for your specific implementation
$sourcePath = '/Server/sites/websiteworkflow';
$defaultUrl = 'https://portal.example.com';

// The first element of the query string will be the site code
$requestSite = $queryArray[0];
// Remove the site code from the array
unset($queryArray[0]);
// The remaining array elements will be the "request tag"
$requestTag = implode($queryArray, ' ');

if ($requestTag == '') {
	// No tag was provided, open the portal URL for the site
	$requestTag = 'portal';
}

// Load the sites XML file
$sitesXml = simplexml_load_file($sourcePath."/sites.xml") or die("Error: Cannot load sites.xml file.");

// Convert XML to json and load into an array
$sitesJson = json_encode($sitesXml);
$siteXmlArray = json_decode($sitesJson,TRUE);

// Load site definitions into an array
if (isset($siteXmlArray['site'])) {
	if (isset($siteXmlArray['site']['site_code'])) {
		$sitesArray[$siteXmlArray['site']['site_code']] = $siteXmlArray['site'];
	} else {
		foreach ($siteXmlArray['site'] as $site) {
			if (isset($site['site_code'])) {
				$sitesArray[$site['site_code']] = $site;
			}
		}
	}
}

// Load the request tags XML file
$requestTagsXml = simplexml_load_file($sourcePath."/requestTags.xml") or die("Error: Cannot load requestTags.xml file.");

// Convert XML to json and load into an array
$requestTagsJson = json_encode($requestTagsXml);
$requestTagsXmlArray = json_decode($requestTagsJson,TRUE);

// Load request tag definitions into an array
if (isset($requestTagsXmlArray['tag'])) {
	foreach ($requestTagsXmlArray['tag'] as $tag) {
		if (isset($tag['code'])) {
		$requestTagsArray[$tag['code']] = $tag;
		}
	}
}

// Find the URL in the site array using the site code and request tag
if (isset($sitesArray[$requestSite][$requestTagsArray[$requestTag]['parent_code']]['url'])) {
	$url = $sitesArray[$requestSite][$requestTagsArray[$requestTag]['parent_code']]['url'];
	echo $url;
} else {
	// Requested URL wasn't found, load the default portal site instead
	echo $defaultUrl;
}
